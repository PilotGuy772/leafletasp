using System.ComponentModel.DataAnnotations;

namespace LeafletAsp.Data;

public record Post(long Time, [Required] string Title, [Required] string Author, [Required] string Content);