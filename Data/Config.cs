using Newtonsoft.Json;

namespace LeafletAsp.Data;

public class Config
{
    public string? DatabaseDir { get; set; }
    public int PostLimit { get; set; }
    public int PostMaximumRetentionDays { get; set; }

    public static Config? ReadFromConfig(string path)
    {
        string json = File.ReadAllText(path);
        Config? config = JsonConvert.DeserializeObject<Config>(json);
        return config;
    }
}