using System.Data.SQLite;
using HtmlAgilityPack;
using Markdig;

namespace LeafletAsp.Data;

// DATABASE SCHEMA IS IN ORDER OF POST RECORD: public record Post(long Id, long Time, string Title, string Author, string Content);


public static class Database
{
    public static string Path = @"URI=file:/home/laeth/leafletasp/base.db"; //update this to match the proper directory later
    private static SQLiteConnection? _con;
    private static SQLiteCommand? _cmd;
    private static SQLiteDataReader? _rdr;
    
    public static void Initialize()
    {
        _con = new SQLiteConnection(Path);
        _cmd = new SQLiteCommand(_con);
        _con.Open();
        _cmd.CommandText = "CREATE TABLE IF NOT EXISTS posts(id INTEGER PRIMARY KEY AUTOINCREMENT, time INT, title TEXT, author TEXT, content TEXT)";
        _cmd.ExecuteNonQuery();
    }

    public static IEnumerable<Post> GetAllPosts()
    {
        if (_cmd == null) throw new NullReferenceException("database is not initialized");
        _cmd.CommandText = "SELECT * FROM posts";
        _rdr = _cmd.ExecuteReader();
        List < Post > posts = new();

        while (_rdr.Read())
        {
            posts.Add(new Post(_rdr.GetInt64(1),
                _rdr.GetString(2),
                _rdr.GetString(3),
                _rdr.GetString(4)
                ));
        }
        
        _rdr.Close();
        return posts;
    }

    public static Post GetPostById(long id)
    {
        if (_cmd == null) throw new NullReferenceException("database is not initialized");
        _cmd.CommandText = "SELECT * FROM posts WHERE id = @id";
        _cmd.Parameters.AddWithValue("@id", id);
        _rdr = _cmd.ExecuteReader();

        if (!_rdr.Read()) throw new ArgumentException("The given post ID does not correspond to a valid post.");
        
        Post ret =  new Post(_rdr.GetInt64(1),
            _rdr.GetString(2),
            _rdr.GetString(3),
            _rdr.GetString(4)
        );
        _rdr.Close();
        return ret;
    }

    public static void SavePost(Post post)
    {
        if (_cmd == null) throw new NullReferenceException("database is not initialized");
        _cmd.CommandText = "INSERT INTO posts(time, title, author, content) VALUES(@time, @title, @author, @content)";
        _cmd.Parameters.AddWithValue("@time", post.Time < 1 ? DateTimeOffset.UtcNow.ToUnixTimeSeconds() : post.Time);
        _cmd.Parameters.AddWithValue("@title", post.Title);
        _cmd.Parameters.AddWithValue("@author", post.Author);
        _cmd.Parameters.AddWithValue("@content", ProcessPostContent(Markdown.ToHtml(post.Content)));
        _cmd.ExecuteNonQuery();
    }
    
    public static async Task SavePostAsync(Post post)
    {
        if (_cmd == null) throw new NullReferenceException("database is not initialized");
        _cmd.CommandText = "INSERT INTO posts(time, title, author, content) VALUES(@time, @title, @author, @content)";
        _cmd.Parameters.AddWithValue("@time", post.Time);
        _cmd.Parameters.AddWithValue("@title", post.Title);
        _cmd.Parameters.AddWithValue("@author", post.Author);
        _cmd.Parameters.AddWithValue("@content", ProcessPostContent(Markdown.ToHtml(post.Content)));
        await _cmd.ExecuteNonQueryAsync();
    }

    private static string ProcessPostContent(string input)
    {

        //first thing's first, go through find and replace dict
        Dictionary<string, string> findAndReplace = new()
        {
            { "\n", "" }
        };

        foreach ((string search, string replace) in findAndReplace)
            input = input.Replace(search, replace);
        
        //define banned attributes
        string[] blockedTags =
        {
            "script",
            "iframe",
            "form",
            "input",
            "textarea",
            "select",
            "label",
            "div",
            "object",
            "style",
            "base",
            "embed",
            "span",
            "param",
            "applet"
        };
        string[] blockedAttributes =
        {
            "onclick",
            "onload",
            "script",
            "style",
            "class",
            "id",
            "name",
            "data",
            "codebase",
            "target"
        };

        //prep html doc
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(input);
        
        //get nodes
        IEnumerable<HtmlNode> nodes = doc.DocumentNode.DescendantsAndSelf();

        IEnumerable<HtmlNode> htmlNodes = nodes as HtmlNode[] ?? nodes.ToArray();
        
        List<HtmlNode> elementsToRemove = htmlNodes.Where(tag => blockedTags.Contains(tag.Name)).ToList();
        
        foreach(HtmlNode element in elementsToRemove)
            element.Remove();


        foreach (HtmlNode node in htmlNodes)
            foreach (string s in blockedAttributes.Intersect(node.Attributes.Select(attribute => attribute.Name))) 
                    node.Attributes.Remove(s);
        
        
        
        return doc.DocumentNode.OuterHtml;
    }
}