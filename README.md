# LeafletAsp

LeafletAsp is a lightweight message board written in ASP.NET Core. It is inspired by [Leaflet](https://leaflet.acei.us) created by Aceius.

### Current Version Information

LeafletAsp is currently in version 0.1.0. It has basic functionality that allows users to post their markdown- or HTML-formatted message to the board and the view it along with other versions. LeafletAsp is mostly intended as an exercise to create a functional ASP application and make it easily deployable and installable. There will likely never be a version 1.0.0.

I intend to add the following features eventually:
* Accounts and authorization
* Account tiers including administrator control over posts
* Integration with a command line tool allowing users to view and post from their command line (potentially integrating SSH key authorization)
* Multiple boards

One of those features is much easier to implement than all the others. but.... whatever. Updates will be pushed as they come and as I choose to work on them. My next goal, though, is to make it reasonably deployable on actual systems and compile some docs.