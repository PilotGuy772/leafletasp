using LeafletAsp.Data;
using Microsoft.AspNetCore.Mvc;
using Database = LeafletAsp.Data.Database;

namespace LeafletAsp.Controllers;

public class HomeController : Controller
{
    [HttpGet]
    public IActionResult Index() => View(Database.GetAllPosts().OrderByDescending(post => post.Time));

    [HttpGet]
    [ActionName("Draft")]
    public IActionResult CreatePost() => View();

    [HttpGet]
    [ActionName("Info")]
    public IActionResult Info() => View();

    [HttpPost]
    [ActionName("Draft")]
    public IActionResult CreatePost( Post post)
    {
        Database.SavePost(post);
        return RedirectToAction("Index");
    }
}