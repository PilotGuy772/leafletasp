using LeafletAsp.Data;

namespace LeafletAsp;

internal static class Program
{
    public static DateTime Startup;

    public static void Main(string[] args)
    {
        Startup = DateTime.UtcNow;
        Database.Initialize();
        Config? config = Config.ReadFromConfig(args.Length > 0 ? args[0] : @"./config.json");
        
        //initialize constant configuration variables
        if (config?.DatabaseDir != null)
            Database.Path = config.DatabaseDir.StartsWith("URI=file:") ? config.DatabaseDir : @"URI=file:" + config.DatabaseDir;
        
        CreateHostBuilder().Build().Run();
    }
    
    public static DateTime UnixTimeStampToDateTime(long unixTimeStamp) => 
        new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
        .AddSeconds( unixTimeStamp )
        .ToLocalTime();
    
    private static IHostBuilder CreateHostBuilder() =>
        Host.CreateDefaultBuilder()
            .ConfigureWebHostDefaults(webBuilder => 
                webBuilder.UseStartup<Startup>());

}