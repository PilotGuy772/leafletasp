namespace LeafletAsp;

public class Startup
{
    public void ConfigureServices(IServiceCollection services) => services.AddControllersWithViews();

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseRouting();
        app.UseEndpoints(end =>
        {
            end.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}"
            );
        });
    }
}